
# 1. 简介
    通用视频下载解析器，是一个通用的视频下载，缩略图获取，缩略图裁剪，视频合成的工具。
    核心类是VideoParser、一组Command和ParseResult；

    源码链接：https://Nulooper@bitbucket.org/Nulooper/generalvideoparser.git

    详情调用示例，可以查看：com.facebook.Test

# 2. 快速使用

## step1: 添加如下依赖

    <dependency>
        <groupId>com.facebook</groupId>
        <artifactId>GeneralVideoParser</artifactId>
        <version>1.1-SNAPSHOT</version>
    </dependency>


## step2: 设置输出格式和输出根目录
    指定输出视频格式和输出根目录，(输出视频格式目前只支持MP4), 然后使用者两个参数实例化VideoParser对象.
        String outputVideoFormat = "mp4";
        String outRootDir = "./test";
        VideoParser videoParser = new VideoParser(outputVideoFormat, outRootDir);


## step3: 执行解析
    调用parse函数，可以执行视频的下载、缩略图裁剪、视频合成.
        VideoParser的parse函数有两个参数，分别是videoId和url，
        videoId:可以随机生成；
        url：指youtube视频的url.
        调用示例如下：
        ParseResult  parseResult = videoParser.parse(videoId, url);

        parse的基本流程：
            1）首先在目录outRootDir下创建一个videoId的子目录：{outRootDir}/{videoId}/；
            2）然后url的下载、缩略图裁剪和视频合成的结果都生成在：{outRootDir}/{videoId}/

## step4: 查看结果
    parse函数的结果会返回一个ParseResult对象parseR   esult.
       对这个parseResult结果的处理流程如下：
       1）将这个对象序列化至日志，以便于事后分析；一定要打这个日志!!!一定要打这个日志!!!一定要打这个日志!!!
       2）调用parseResult.isSuccess(),判断当前parse是否成功；
       3）如果parse成功，则可以通过如下几个方法获取对一个的文件：
            1]parseResult.getVideoPath(): 获取合成后的视频文件路径；
            2]parseResult.getLargeCoverImagePath(): 获取缩略图的大封面图；
            3]parseResult.getSmallCoverImagePath(): 获取缩略图的小封面图;
            4]parseResult.getExecuteResultList(): 可以获取当前解析步骤每一个步骤的执行细节;


