package com.facebook.parse;

import com.facebook.command.ExecuteResult;
import com.facebook.command.Status;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * 解析结果.
 * <p>
 * Created by zhiwei on 2018/1/10.
 */
public class ParseResult implements Serializable {
    private String id;

    /*
    原始下载视频路径.
     */
    private String originalVideoPath;
    /*
    原始下载图片路径.
     */
    private String originalImagePath;

    /*
    合成的视频路径.
     */
    private String videoPath;
    /*
    裁剪后的大图路径.
     */
    private String largeCoverImagePath;
    /*
    裁剪后的小图路径.
     */
    private String smallCoverImagePath;

    /*
    解析结果.
     */
    private Status status;

    /*
    解析开始时间戳.
     */
    private long timestamp;

    /*
    当前解析每一个指令执行的结果.
     */
    private List<ExecuteResult> executeResultList;


    public boolean isSuccess() {
        return null != originalVideoPath &&
                null != originalImagePath &&
                null != videoPath &&
                null != largeCoverImagePath &&
                null != smallCoverImagePath;
    }

    /*
    当前解析耗时（单位：毫秒）
     */
    private long duration;//执行解析耗时.

    public ParseResult(String originalVideoPath, String originalImagePath, String videoPath,
                       String largeCoverImagePath, String smallCoverImagePath, Status status) {
        this.id = UUID.randomUUID().toString();
        this.originalVideoPath = originalVideoPath;
        this.originalImagePath = originalImagePath;
        this.videoPath = videoPath;
        this.largeCoverImagePath = largeCoverImagePath;
        this.smallCoverImagePath = smallCoverImagePath;
        this.status = status;
        this.timestamp = System.currentTimeMillis();
    }

    public ParseResult(String videoPath, String largeCoverImagePath,
                       String smallCoverImagePath, Status status) {
        this.id = UUID.randomUUID().toString();
        this.videoPath = videoPath;
        this.largeCoverImagePath = largeCoverImagePath;
        this.smallCoverImagePath = smallCoverImagePath;
        this.status = status;
        this.timestamp = System.currentTimeMillis();
    }

    public ParseResult() {
        this.id = UUID.randomUUID().toString();
        this.timestamp = System.currentTimeMillis();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getLargeCoverImagePath() {
        return largeCoverImagePath;
    }

    public void setLargeCoverImagePath(String largeCoverImagePath) {
        this.largeCoverImagePath = largeCoverImagePath;
    }

    public String getSmallCoverImagePath() {
        return smallCoverImagePath;
    }

    public void setSmallCoverImagePath(String smallCoverImagePath) {
        this.smallCoverImagePath = smallCoverImagePath;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getOriginalVideoPath() {
        return originalVideoPath;
    }

    public void setOriginalVideoPath(String originalVideoPath) {
        this.originalVideoPath = originalVideoPath;
    }

    public String getOriginalImagePath() {
        return originalImagePath;
    }

    public void setOriginalImagePath(String originalImagePath) {
        this.originalImagePath = originalImagePath;
    }

    public void addExecuteResult(ExecuteResult executeResult) {
        if (null == this.executeResultList) {
            this.executeResultList = new ArrayList<>();
        }
        this.executeResultList.add(executeResult);
    }

    public List<ExecuteResult> getExecuteResultList() {
        return executeResultList;
    }

    public void setExecuteResultList(List<ExecuteResult> executeResultList) {
        this.executeResultList = executeResultList;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "ParseResult{" +
                "id='" + id + '\'' +
                ", originalVideoPath='" + originalVideoPath + '\'' +
                ", originalImagePath='" + originalImagePath + '\'' +
                ", videoPath='" + videoPath + '\'' +
                ", largeCoverImagePath='" + largeCoverImagePath + '\'' +
                ", smallCoverImagePath='" + smallCoverImagePath + '\'' +
                ", status=" + status +
                ", timestamp=" + timestamp +
                ", executeResultList=" + executeResultList +
                ", duration=" + duration +
                '}';
    }
}
