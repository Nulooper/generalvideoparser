package com.facebook.parse;

import com.facebook.command.*;
import com.facebook.utils.Constants;
import com.facebook.utils.Utils;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 视频解析器.
 */
public class VideoParser implements Serializable {
    /*
    视频输出格式.
     */
    private String outVideoFormat;

    /*
    解析结果根目录.
     */
    private String outParseRootDir;

    public VideoParser(String outVideoFormat, String outParseRootDir) {
        this.outVideoFormat = outVideoFormat;
        this.outParseRootDir = outParseRootDir;
        if (!this.outParseRootDir.endsWith("/")) {
            this.outParseRootDir = this.outParseRootDir + "/";
        }
    }

    public String getOutVideoFormat() {
        return outVideoFormat;
    }

    public void setOutVideoFormat(String outVideoFormat) {
        this.outVideoFormat = outVideoFormat;
    }

    public String getOutParseRootDir() {
        return outParseRootDir;
    }

    public void setOutParseRootDir(String outParseRootDir) {
        this.outParseRootDir = outParseRootDir;
        if (!this.outParseRootDir.endsWith("/")) {
            this.outParseRootDir = this.outParseRootDir + "/";
        }
    }

    /**
     * 解析给定视频url.
     * 解析成功需要同时满足以下条件：
     * 1)指定id目录下有对应的大缩略图；
     * 2）指定id目录下有对应的小缩略图；
     * 3）指定id目录下有合成后的视频；
     * <p>
     * 函数返回结果：
     * -1：下载视频失败；
     * -2：下载视频成功，但是图片裁剪失败；
     * -3：下载视频和图片裁剪成功，但是合成视频失败；
     * -4：未知异常，没有生成指定的三个文件；需要记录日志用于分析；
     *
     * @param videoId
     * @param videoUrl
     * @return
     */
    public ParseResult parse(String videoId, String videoUrl) throws IOException {
        String outDir = this.outParseRootDir + videoId + "/";
        Utils.mkdirs(outDir);
        ParseResult parseResult = new ParseResult();
        String originalVideoPath;//原始视频路径.
        String originalImagePath;//原始图片路径.
        VideoFrame originalVideoFrame;//原始视频帧宽度和高度.
        String scaledSuffixVideoPath;//对宣传视频放缩后的视频路径.
        String originalSuffixVideoPath =
                Constants.VIDEO_FRAME_SUFFIX_VIDEO_PATH_MAP.get(new VideoFrame(1280, 720));
        String concatedVideoPath;//合成后的视频路径
        String croppedLargeCoverImagePath;//裁剪后的大图路径.
        String croppedSmallCoverImagePath;//裁剪后的小图路径.

        try {
            //首先构建一组命令.
            //step1: 下载视频和缩略图.
            DownloadCommand downloadCommand = CommandBuilder.buildParseCommand(videoId,
                    videoUrl, outVideoFormat, outDir);
            System.out.println("start to exe [" + downloadCommand.toShCmd() + "]");
            ExecuteResult downloadExecuteResult = downloadCommand.execute();
            parseResult.addExecuteResult(downloadExecuteResult);

            originalImagePath = downloadCommand.getThumbnailPath();
            originalVideoPath = downloadCommand.getVideoPath();
            parseResult.setOriginalImagePath(originalImagePath);
            parseResult.setOriginalVideoPath(originalVideoPath);


            if (downloadExecuteResult.isSuccess()) {
                System.out.println(downloadCommand.getClass().getName() + ":\tOK !");

                //视频下载成功，//step3: 构建视频处理命令.
                VideoInformationCommand videoInformationCommand = new VideoInformationCommand(originalVideoPath);
                System.out.println("start to exe  [" + videoInformationCommand.toShCmd() + "]");
                ExecuteResult videoInformationExecuteResult = videoInformationCommand.execute();
                parseResult.addExecuteResult(videoInformationExecuteResult);
                originalVideoFrame = videoInformationCommand.getVideoFrame();

                if (videoInformationExecuteResult.isSuccess()) {
                    System.out.println(videoInformationCommand.getClass().getName() + ":\tOK !");

                    //获取视频信息成功,开始放缩宣传视频.
                    VideoScaleCommand videoScaleCommand =
                            new VideoScaleCommand(originalSuffixVideoPath, originalVideoFrame, outDir);
                    System.out.println("start to exe [" + videoScaleCommand.toShCmd() + "]");
                    ExecuteResult videoScaleExecuteResult = videoScaleCommand.execute();
                    parseResult.addExecuteResult(videoScaleExecuteResult);
                    scaledSuffixVideoPath = videoScaleCommand.getOutputPath();

                    if (videoScaleExecuteResult.isSuccess()) {
                        System.out.println(videoScaleCommand.getClass().getName() + ":\tOK !");

                        //视频放缩成功, 构建视频合成命令.
                        List<String> inputVideoPathList = new ArrayList<>();
                        inputVideoPathList.add(originalVideoPath);
                        inputVideoPathList.add(scaledSuffixVideoPath);
                        concatedVideoPath = this.getConcateVideoPath(originalVideoPath);
                        parseResult.setVideoPath(concatedVideoPath);
                        System.out.println(inputVideoPathList);
                        System.out.println(concatedVideoPath);
                        VideoConcatCommand videoConcatCommand = new VideoConcatCommand(inputVideoPathList,
                                concatedVideoPath);
                        System.out.println("start to exe [" + videoConcatCommand.toShCmd() + "]");
                        ExecuteResult concateExeresult = videoConcatCommand.execute();
                        parseResult.addExecuteResult(concateExeresult);

                        if (concateExeresult.isSuccess()) {
                            //视频拼接成功.执行图片裁剪.
                            System.out.println(videoConcatCommand.getClass().getName() + ":\tOK !");

                            //下载成功，//step2: 构建一组图片处理命令.
                            List<CropImageCommand> cropImageCommandList =
                                    CommandBuilder.buildCropImageCommandList(downloadCommand.getThumbnailPath());
                            for (CropImageCommand cropImageCommand : cropImageCommandList) {
                                System.out.println("start to exe [" + cropImageCommand.toShCmd() + "]");
                                ExecuteResult cropResult = cropImageCommand.execute();
                                parseResult.addExecuteResult(cropResult);
                                if (cropResult.isSuccess()) {
                                    if ("large".equals(cropImageCommand.getType())) {
                                        croppedLargeCoverImagePath = cropImageCommand.getOutputImagePath();
                                        parseResult.setLargeCoverImagePath(cropImageCommand.getOutputImagePath());
                                    } else {
                                        croppedSmallCoverImagePath = cropImageCommand.getOutputImagePath();
                                        parseResult.setSmallCoverImagePath(cropImageCommand.getOutputImagePath());
                                    }
                                    System.out.println(cropImageCommand.getClass().getName() + ":OK !");
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            Status status = new Status(-1, e.toString());
            parseResult.setStatus(status);
        }

        if (!parseResult.isSuccess()) {
            Status status = new Status(-1, "解析失败，详情请查看executeResultList.");
            parseResult.setStatus(status);
        } else {
            Status status = new Status(0, "OK");
            parseResult.setStatus(status);
        }

        return parseResult;
    }


    private String getConcateVideoPath(String originalVideoPath) {
        System.out.println(this.getClass().getName() + ":getConcateVideoPath originalVideoPath[" + originalVideoPath + "]");
        int index = originalVideoPath.lastIndexOf(".");
        return originalVideoPath.substring(0, index) + "_concate." + originalVideoPath.substring(index + 1);
    }

    /**
     * 检查执行parse命令后，指定目录下是否存在要求的三个文件.
     * 如果存在，返回true；如果不存在, 返回false，并且删除该视频目录；
     *
     * @return
     */
    private boolean postCheckAfterParse(String videoId) {
        String toCheckDir = this.outParseRootDir + videoId;
        return false;
    }


    public static void main(String[] args) {

    }
}
