package com.facebook.shell;

import com.facebook.command.VideoFrame;
import com.facebook.utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ShellHelper {

    public static final String SHELL_VIDEO_SCALE_CMD = "ffmpeg -i INPUT_PATH -vf scale=VIDEO_FRAME -y OUTPUT_PATH";

    public static final String SHELL_VIDEO_CONCATE_CMD = "./mmcat INPUT_PATH SUFFIX_PATH OUTPUT_PATH";

    /**
     * 创建视频放缩shell命令.
     *
     * @param inputPath
     * @param videoFrame
     * @param outputPath
     * @return
     */
    public static String createScaleVideoShellCmd(String inputPath, VideoFrame videoFrame, String outputPath) {
        return SHELL_VIDEO_SCALE_CMD.replace("INPUT_PATH", inputPath)
                .replace("VIDEO_FRAME", videoFrame.toString())
                .replace("OUTPUT_PATH", outputPath);
    }

    public static String createConcateVideoShellCmd(String inputVideoPath, String suffixVideoPath, String outputPath) {
        return SHELL_VIDEO_CONCATE_CMD.replace("INPUT_PATH", inputVideoPath)
                .replace("SUFFIX_PATH", suffixVideoPath)
                .replace("OUTPUT_PATH", outputPath);
    }


    /**
     * 执行shell脚本并且获取执行结果.
     *
     * @param shellCmd
     * @return
     */
    public static List<String> exeShell(String shellCmd) {

        List<String> resultLineList = new ArrayList<String>();

        BufferedReader errorBr = null;

        BufferedReader consoleBr = null;

        try {
            Process p = Runtime.getRuntime().exec(shellCmd);

            errorBr = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            String line = null;
            while ((line = errorBr.readLine()) != null) {
                resultLineList.add(line);
            }

            consoleBr = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = consoleBr.readLine()) != null) {
                System.out.println(line);
            }

            int exitVal = p.waitFor();

            System.out.println("Process exitValue: " + exitVal);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != errorBr) {
                try {
                    errorBr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (null != consoleBr) {
                try {
                    consoleBr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


        return resultLineList;
    }


    public static VideoFrame parseForVideoFrame(List<String> resultLineList) {
        VideoFrame videoFrame = null;
        String videoFrameStr = null;
        Pattern aspectRatioPattern = Pattern.compile("\\d{3,}x\\d{3,}");
        Pattern codecLinePattern = Pattern.compile(".*Video.*SAR.*DAR.*");
        if (null != resultLineList) {
            for (String line : resultLineList) {
                line = line.trim();
                Matcher matcher = codecLinePattern.matcher(line);
                if (matcher.matches()) {
                    videoFrameStr = Utils.extractFirstSubStr(aspectRatioPattern, line);
                }
            }
        }

        if (null != videoFrameStr) {
            String[] items = videoFrameStr.split("x");
            if (items.length == 2) {
                try {
                    int width = Integer.parseInt(items[0]);
                    int height = Integer.parseInt(items[1]);
                    videoFrame = new VideoFrame(width, height);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return videoFrame;
    }


    public static void main(String[] args) throws Exception {
        ProcessBuilder processBuilder = new ProcessBuilder("python", "--version");
        System.out.println(processBuilder.toString());
        Process process = processBuilder.start();
        int value = process.waitFor();
        System.out.println(value);
    }

}
