package com.facebook;

import com.facebook.parse.ParseResult;
import com.facebook.parse.VideoParser;
import com.facebook.shell.ShellHelper;
import com.google.gson.Gson;

import java.io.IOException;

public class Test {
    public static void main(String[] args) throws IOException {


        //ShellHelper.exeShell("youtube-dl --write-thumbnail --write-description --recode-video mp4 https://www.youtube.com/watch?v=4_Zmt9gSTso");

        //args = new String[]{"./","v1","https://www.youtube.com/watch?v=4_Zmt9gSTso"};
        String outputRootDir = args[0];
        VideoParser videoParser = new VideoParser("mp4", outputRootDir);
        String videoId = args[1];

        //ShellHelper.exeShell("mv *.mp4 " + videoId + ".mp4");
        //ShellHelper.exeShell("mv *.jpg" + videoId + ".jpg");
        String videoUrl = args[2];
        ParseResult parseResult = videoParser.parse(videoId, videoUrl);
        Gson gson = new Gson();
        String json = gson.toJson(parseResult);
        System.out.println(json);
    }
}
