package com.facebook.utils;

import com.facebook.command.VideoFrame;

import java.util.HashMap;
import java.util.Map;

public class Constants {

    public static final String SHELL_SCRIPTS_ROOT_DIR = "/home/wangzhiwei/service/video-standard/scripts/";

    public static final String STANDARD_SUFFIX_VIDEO_ROOT_DIR = "/home/wangzhiwei/service/video-standard/suffix_videos/";

    public static final Map<VideoFrame, String> VIDEO_FRAME_SUFFIX_VIDEO_PATH_MAP = new HashMap<>();

    static {
        VIDEO_FRAME_SUFFIX_VIDEO_PATH_MAP.put(new VideoFrame(426, 240),
                STANDARD_SUFFIX_VIDEO_ROOT_DIR + "426_240.mp4");
        VIDEO_FRAME_SUFFIX_VIDEO_PATH_MAP.put(new VideoFrame(640, 360),
                STANDARD_SUFFIX_VIDEO_ROOT_DIR + "640_360.mp4");
        VIDEO_FRAME_SUFFIX_VIDEO_PATH_MAP.put(new VideoFrame(854, 480),
                STANDARD_SUFFIX_VIDEO_ROOT_DIR + "854_480.mp4");
        VIDEO_FRAME_SUFFIX_VIDEO_PATH_MAP.put(new VideoFrame(1280, 720),
                STANDARD_SUFFIX_VIDEO_ROOT_DIR + "1280_720.mp4");
        VIDEO_FRAME_SUFFIX_VIDEO_PATH_MAP.put(new VideoFrame(1920, 1080),
                STANDARD_SUFFIX_VIDEO_ROOT_DIR + "1920_1080.mp4");
    }
}
