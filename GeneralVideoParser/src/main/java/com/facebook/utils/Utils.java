package com.facebook.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    public static void mkdirs(String dirPath) throws IOException {
        if (!Files.exists(Paths.get(dirPath))) {
            Files.createDirectories(Paths.get(dirPath));
        }
    }

    /**
     * 根据pattern，从源文本sourceText中抽取第一个子串.
     *
     * @param pattern
     * @param sourceText
     * @return
     */
    public static String extractFirstSubStr(Pattern pattern, String sourceText) {
        String result = null;
        if (null != sourceText && null != pattern) {
            Matcher matcher = pattern.matcher(sourceText);
            if (matcher.find()) {
                result = matcher.group(0);
            }
        }

        return result;
    }

    public static String getSuffixOfFilePath(String filePath) {
        String suffix = null;

        if (null != filePath) {
            int index = filePath.lastIndexOf(".");
            if (index >= 0 && filePath.length() > 1) {
                suffix = filePath.substring(index + 1).toLowerCase();
            }
        }

        return suffix;
    }

    public static String joinOn(List<String> itemList, String on) {
        if (null == itemList || itemList.isEmpty()) {
            return "";
        }

        int size = itemList.size();
        if (1 == size) {
            return itemList.get(0);
        }


        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size - 1; ++i) {
            sb.append(itemList.get(i)).append(on);
        }
        sb.append(itemList.get(size - 1));


        return sb.toString();
    }

    public static void main(String[] args) throws IOException {
        System.out.println("HELLO WORLD !");
    }

}
