package com.facebook.command;

import com.facebook.shell.ShellHelper;

import java.io.Serializable;
import java.util.List;

/**
 * 获取视频信息命令.
 * Created by zhiwei on 2018/1/10.
 */
public class VideoInformationCommand implements Executable, Serializable {

    private static final String SH_PREFIX = "ffmpeg -i ";

    private String inputVideoPath;

    private VideoFrame videoFrame;

    public VideoInformationCommand(String inputVideoPath) {
        this.inputVideoPath = inputVideoPath;
    }

    public VideoInformationCommand() {
    }

    @Override
    public ExecuteResult execute() {
        String shCmd = this.toShCmd();
        List<String> errorLineList = ShellHelper.exeShell(shCmd);
        VideoFrame videoFrame = ShellHelper.parseForVideoFrame(errorLineList);
        this.videoFrame = videoFrame;
        ExecuteResult executeResult = new ExecuteResult(this.getClass().getName(), shCmd);
        executeResult.setErrorLineList(errorLineList);

        if (null == videoFrame) {
            executeResult.setStatus(new Status(-1, "获取VideoFrame信息失败 ! "));
        } else {
            executeResult.setStatus(new Status(0, "OK"));
            executeResult.putExtra("videoFrom", this.videoFrame);
        }

        return executeResult;
    }

    @Override
    public String toShCmd() {
        return SH_PREFIX + this.inputVideoPath;
    }


    public String getInputVideoPath() {
        return inputVideoPath;
    }

    public void setInputVideoPath(String inputVideoPath) {
        this.inputVideoPath = inputVideoPath;
    }

    public VideoFrame getVideoFrame() {
        return videoFrame;
    }

    public void setVideoFrame(VideoFrame videoFrame) {
        this.videoFrame = videoFrame;
    }

    @Override
    public String toString() {
        return "VideoInformationCommand{" +
                "inputVideoPath='" + inputVideoPath + '\'' +
                ", videoFrame=" + videoFrame +
                '}';
    }
}
