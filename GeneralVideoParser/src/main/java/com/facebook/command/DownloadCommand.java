package com.facebook.command;

import com.facebook.shell.ShellHelper;

import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

/**
 * 视频解析命令.
 */
public class DownloadCommand implements Serializable, Executable {

    public static final String WHITE_SPACE = " ";

    public static final String BASE_COMMAND = "youtube-dl";

    /*
    下载缩略图.
     */
    public static final String ARGUMENT_WRITE_THUMBNAIL = "--write-thumbnail";//--write-thumbnail

    /*
    下载视频描述.
     */
    public static final String ARGUMENT_WRITE_DESCRIPTION = "--write-description";//--write-description

    /*
    重新编码视频格式.
     */
    public static final String ARGUMENT_RECODE_VIDEO = "--recode-video";//--recode-video

    /*
    输出文件名模板.
     */
    public static final String OUTPUT_FILENAME_TEMPLATE = ".%(ext)s";//

    public static final String ARGUMENT_OUTPUT = "-o";

    /*
    输出目录.
     */
    private String outDir;

    /*
    输出视频格式.
     */
    private String outVideoFormat;

    private String url;

    private String videoId;


    public DownloadCommand(String videoId, String url, String outVideoFormat, String outDir) {
        this.videoId = videoId;
        this.outDir = outDir;
        if (!this.outDir.endsWith("/")) {
            this.outDir = this.outDir + "/";
        }

        this.outVideoFormat = outVideoFormat;
        this.url = url;
    }

    public DownloadCommand() {
    }

    @Override
    public ExecuteResult execute() {
        String cmd = this.toShCmd();
        List<String> errorLineList = ShellHelper.exeShell(cmd);

        ExecuteResult executeResult = new ExecuteResult(this.getClass().getName(), cmd);
        executeResult.setErrorLineList(errorLineList);
        Status status = new Status(0, "OK");

        //判断是否执行成功.检测缩略图和视频文件是否存在.
        boolean isThumbnailExists = Files.exists(Paths.get(this.getThumbnailPath()));
        boolean isVideoExists = Files.exists(Paths.get(this.getVideoPath()));
        if (isThumbnailExists && isVideoExists) {
            //执行成功.
        } else {
            StringBuilder messageSB = new StringBuilder();
            if (!isThumbnailExists) {
                //缩略图下载失败.
                messageSB.append("缩略图[" + this.getThumbnailPath() + "]下载失败!").append(";");
            }
            if (!isVideoExists) {
                //视频下载失败.
                messageSB.append("视频[" + this.getVideoPath() + "]下载失败");
            }
            status = new Status(-1, messageSB.toString());
            executeResult.setStatus(status);
        }

        return executeResult;
    }

    @Override
    public String toShCmd() {

        StringBuilder shSB = new StringBuilder();
        shSB.append(BASE_COMMAND).append(WHITE_SPACE);
        shSB.append(ARGUMENT_WRITE_THUMBNAIL).append(WHITE_SPACE);
        shSB.append(ARGUMENT_WRITE_DESCRIPTION).append(WHITE_SPACE);
        shSB.append(ARGUMENT_OUTPUT).append(WHITE_SPACE)//.append("\"")
                .append(this.outDir)
                .append(this.videoId)
                .append(OUTPUT_FILENAME_TEMPLATE)
                .append(WHITE_SPACE);
        shSB.append(ARGUMENT_RECODE_VIDEO).append(WHITE_SPACE).append(outVideoFormat).append(WHITE_SPACE);
        shSB.append(this.url);

        return shSB.toString();
    }


    public String getOutDir() {
        return outDir;
    }

    public void setOutDir(String outDir) {
        this.outDir = outDir;
        if (!this.outDir.endsWith("/")) {
            this.outDir = this.outDir + "/";
        }
    }

    public String getOutVideoFormat() {
        return outVideoFormat;
    }

    public void setOutVideoFormat(String outVideoFormat) {
        this.outVideoFormat = outVideoFormat;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getVideoId() {

        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }


    public String getVideoFilename() {
        return this.videoId + "." + this.outVideoFormat;
    }

    public String getVideoPath() {
        return this.outDir + this.getVideoFilename();
    }

    public String getThumbnailFilename() {
        return this.videoId + ".jpg";
    }

    public String getThumbnailPath() {
        return this.outDir + this.getThumbnailFilename();
    }

    public static void main(String[] args) {
        DownloadCommand parseCommand = new DownloadCommand();
        parseCommand.setVideoId(UUID.randomUUID().toString());
        parseCommand.setOutDir("/home/wangzhiwei/service/youtube/test");
        parseCommand.setOutVideoFormat("mp4");
        parseCommand.setUrl("https://www.youtube.com/watch?v=4_Zmt9gSTso");

        String sh = parseCommand.toShCmd();

        System.out.println(sh);

        System.out.println(parseCommand.getVideoFilename());

        System.out.println(parseCommand.getVideoPath());

        System.out.println(parseCommand.getThumbnailPath());
    }
}
