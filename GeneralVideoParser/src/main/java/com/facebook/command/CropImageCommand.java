package com.facebook.command;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 图片裁剪命令.
 */
public class CropImageCommand implements Serializable, Executable {

    private int x;
    private int y;
    private int width;
    private int height;

    private String originalImagePath;
    private String outputImagePath;

    private String formatName = "jpg";

    private String type;

    public CropImageCommand() {
    }

    public CropImageCommand(int x, int y, int width, int height, String originalImagePath,
                            String outputImagePath, String formatName) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.originalImagePath = originalImagePath;
        this.outputImagePath = outputImagePath;
        this.formatName = formatName;
    }

    public CropImageCommand(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    @Override
    public ExecuteResult execute() {
        ExecuteResult executeResult = new ExecuteResult(this.getClass().getName(), null);

        try {
            File originalFile = new File(this.originalImagePath);
            BufferedImage imageFirst = ImageIO.read(originalFile);
            BufferedImage croppedImage = imageFirst.getSubimage(this.x, this.y, this.width, this.height);
            ImageIO.write(croppedImage, formatName, new FileOutputStream(this.getOutputImagePath()));
            executeResult.putExtra("outputImagePath", this.getOutputImagePath());
        } catch (IOException e) {
            List<String> errorLineList = new ArrayList<>();
            errorLineList.add(e.toString());
            executeResult.setErrorLineList(errorLineList);

            Status status = new Status(-1,
                    "裁剪图片[" + this.originalImagePath + "]异常[" + e.toString() + "].");
            executeResult.setStatus(status);
        }

        return executeResult;
    }

    @Override
    public String toShCmd() {
        return null;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getOriginalImagePath() {
        return originalImagePath;
    }

    public String getFormatName() {
        return formatName;
    }

    public void setFormatName(String formatName) {
        this.formatName = formatName;
    }

    public void setOriginalImagePath(String originalImagePath) {
        this.originalImagePath = originalImagePath;
    }

    public String getOutputImagePath() {
        int index = this.originalImagePath.lastIndexOf(".");
        String prefix = this.originalImagePath.substring(0, index) + "_" + this.type;
        String suffix = this.originalImagePath.substring(index + 1);

        return prefix + "." + suffix;
    }

    public void setOutputImagePath(String outputImagePath) {
        this.outputImagePath = outputImagePath;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "CropImageCommand{" +
                "x=" + x +
                ", y=" + y +
                ", width=" + width +
                ", height=" + height +
                ", originalImagePath='" + originalImagePath + '\'' +
                ", outputImagePath='" + outputImagePath + '\'' +
                ", formatName='" + formatName + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    public static void main(String[] args) {

    }


}
