package com.facebook.command;

import com.facebook.utils.Utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CommandBuilder {

    public static final int LARGE_IMAGE_WIDTH = 900;
    public static final int LARGE_IMAGE_HEIGHT = 500;
    public static final double LARGE_WIDTH_HEIGHT_RATIO = LARGE_IMAGE_WIDTH * 1.0 / LARGE_IMAGE_HEIGHT;
    public static final int SMALL_IMAGE_WIDTH = 320;
    public static final int SMALL_IMAGE_HEIGHT = 320;
    public static final double SMALL_WIDTH_HEIGHT_RATIO = SMALL_IMAGE_WIDTH * 1.0 / SMALL_IMAGE_HEIGHT;

    public static DownloadCommand buildParseCommand(String videoId, String url, String outVideoFormat, String outDir) {
        return new DownloadCommand(videoId, url, outVideoFormat, outDir);
    }


    public static VideoConcatCommand buildConcatVideoCommand(String inputVideoPath1, String inputVideoPath2,
                                                             String outputConcatedPath) {
        return null;
    }

    public static List<CropImageCommand> buildCropImageCommandList(String inputImagePath) throws IOException {
        List<CropImageCommand> cropImageCommandList = new ArrayList<CropImageCommand>();

        String formatName = Utils.getSuffixOfFilePath(inputImagePath);
        String outPath = inputImagePath.substring(0, inputImagePath.lastIndexOf(".")) + "." + formatName;
        BufferedImage originalImage = ImageIO.read(new File(inputImagePath));
        int originalWidth = originalImage.getWidth();
        int originalHeight = originalImage.getHeight();

        CropImageCommand largeCropImageCommand = buildCropImageCommand(originalWidth, originalHeight,
                "large", inputImagePath, outPath, formatName);
        cropImageCommandList.add(largeCropImageCommand);
        CropImageCommand smallCropImageCommand = buildCropImageCommand(originalWidth, originalHeight,
                "small", inputImagePath, outPath, formatName);
        cropImageCommandList.add(smallCropImageCommand);

        return cropImageCommandList;
    }

    /**
     * 根据原始图片宽度和高度计算图片裁剪参数.
     *
     * @param originalWidth  原始图片的宽度
     * @param originalHeight 原始图片的高度
     * @param type           类型：large/small.
     * @return
     */
    public static CropImageCommand buildCropImageCommand(int originalWidth, int originalHeight, String type,
                                                         String originalImagePath, String outputImagePath, String formatName) {
        CropImageCommand cropImageCommand = new CropImageCommand(0, 0, originalWidth, originalHeight);
        cropImageCommand.setType(type);

        int targetWidth = originalWidth;
        int targetHeight = originalHeight;


        //计算裁剪参数.

        //先根据最小值找参考项.
        String reference = "width";
        if (originalHeight < originalWidth) {
            reference = "height";
        }


        //计算最终宽度和高度.
        if ("large".equals(type)) {
            //large.
            if (originalHeight >= LARGE_IMAGE_HEIGHT && originalWidth >= LARGE_IMAGE_WIDTH) {
                targetWidth = LARGE_IMAGE_WIDTH;
                targetHeight = LARGE_IMAGE_HEIGHT;
            } else {
                double originalWidthHeightRatio = originalWidth * 1.0 / originalHeight;
                if (Double.compare(originalWidthHeightRatio, LARGE_WIDTH_HEIGHT_RATIO) < 0) {
                    targetWidth = originalWidth;
                    targetHeight = (int) (originalWidth / LARGE_WIDTH_HEIGHT_RATIO);
                } else {
                    if ("width".equals(reference)) {
                        targetHeight = originalWidth * 5 / 9;
                    } else {
                        targetWidth = originalHeight * 9 / 5;
                    }
                }
            }

        } else {
            //small.
            if (originalWidth >= 320 && originalHeight >= 320) {
                targetWidth = 320;
                targetHeight = 320;
            } else {
                if ("width".equals(reference)) {
                    targetHeight = originalWidth;
                } else {
                    targetWidth = originalHeight;
                }
            }
        }

        //计算裁剪坐标.
        int x = (int) (originalWidth * 1.0 / 2 - targetWidth * 1.0 / 2);
        int y = (int) (originalHeight * 1.0 / 2 - targetHeight * 1.0 / 2);

        //构建裁剪对象.
        cropImageCommand.setX(x);
        cropImageCommand.setY(y);
        cropImageCommand.setWidth(targetWidth);
        cropImageCommand.setHeight(targetHeight);
        cropImageCommand.setFormatName(formatName);
        cropImageCommand.setOriginalImagePath(originalImagePath);
        cropImageCommand.setOutputImagePath(outputImagePath);

        return cropImageCommand;
    }


    public static void main(String[] args) throws IOException {
        String path = "tmp.jpg";
        List<CropImageCommand> cropImageCommandList = buildCropImageCommandList(path);
        for (CropImageCommand cropImageCommand : cropImageCommandList) {
            System.out.println(cropImageCommand);
        }

    }
}
