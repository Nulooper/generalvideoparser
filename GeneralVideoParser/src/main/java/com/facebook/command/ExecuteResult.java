package com.facebook.command;

import java.io.Serializable;
import java.util.*;

/**
 * Created by zhiwei on 2018/1/10.
 */
public class ExecuteResult implements Serializable {
    private String id;
    private String shCmd;
    private List<String> errorLineList;
    private long timestamp;
    private Status status = new Status(0, "OK");
    private String commandClassName;

    private Map<String, Object> extraMap = new HashMap<String, Object>();

    public ExecuteResult(String commandClassName, String shCmd) {
        this.id = UUID.randomUUID().toString();
        this.commandClassName = commandClassName;
        this.shCmd = shCmd;
        this.errorLineList = new ArrayList<>();
        this.timestamp = System.currentTimeMillis();
    }

    public ExecuteResult() {
        this.id = UUID.randomUUID().toString();
        this.errorLineList = new ArrayList<>();
        this.timestamp = System.currentTimeMillis();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShCmd() {
        return shCmd;
    }

    public void setShCmd(String shCmd) {
        this.shCmd = shCmd;
    }

    public List<String> getErrorLineList() {
        return errorLineList;
    }

    public void setErrorLineList(List<String> errorLineList) {
        this.errorLineList = errorLineList;
    }

    public void addErrorLine(String errorLine) {
        if (null == this.errorLineList) {
            this.errorLineList = new ArrayList<>();
        }
        this.errorLineList.add(errorLine);
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public boolean isSuccess() {
        return null == this.status || this.status.getCode() >= 0;
    }

    public String getCommandClassName() {
        return commandClassName;
    }

    public void setCommandClassName(String commandClassName) {
        this.commandClassName = commandClassName;
    }

    public void putExtra(String key, Object value) {
        if (null == this.extraMap) {
            this.extraMap = new HashMap<>();
        }
        this.extraMap.put(key, value);
    }

    public Map<String, Object> getExtraMap() {
        return extraMap;
    }

    public void setExtraMap(Map<String, Object> extraMap) {
        this.extraMap = extraMap;
    }

    @Override
    public String toString() {
        return "ExecuteResult{" +
                "id='" + id + '\'' +
                ", shCmd='" + shCmd + '\'' +
                ", errorLineList=" + errorLineList +
                ", timestamp=" + timestamp +
                ", status=" + status +
                '}';
    }
}
