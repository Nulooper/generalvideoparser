package com.facebook.command;

import com.facebook.shell.ShellHelper;

import java.io.File;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * 视频放缩命令.
 * Created by zhiwei on 2018/1/10.
 */
public class VideoScaleCommand implements Executable, Serializable {

    public static final String SHELL_VIDEO_SCALE_CMD = "ffmpeg -i INPUT_PATH -vf scale=VIDEO_FRAME -y OUTPUT_PATH";

    public static final String SCALE_SUFFIX = "_scale";

    private String inputVideoPath;

    private VideoFrame videoFrame;

    private String outDir;

    public VideoScaleCommand(String inputVideoPath, VideoFrame videoFrame, String outDir) {
        this.inputVideoPath = inputVideoPath;
        this.videoFrame = videoFrame;

        this.outDir = outDir;
        if (!outDir.endsWith("/")) {
            this.outDir = this.outDir + "/";
        }
    }

    public VideoScaleCommand() {
    }

    @Override
    public ExecuteResult execute() {
        String shCmd = this.toShCmd();
        ExecuteResult executeResult = new ExecuteResult(this.getClass().getName(), shCmd);

        List<String> errorLineList = ShellHelper.exeShell(shCmd);
        executeResult.setErrorLineList(errorLineList);

        boolean isVideoExists = Files.exists(Paths.get(this.getOutputPath()));
        if (isVideoExists) {
            //执行成功.
            executeResult.putExtra("output_scaled_path", this.getOutputPath());
        } else {
            Status status = new Status(-1, "视频[" + this.inputVideoPath + "]放缩失败 !");
            executeResult.setStatus(status);
        }

        return executeResult;
    }

    public String getOutputPath() {
        String filename = new File(this.inputVideoPath).getName();
        String[] items = filename.split("\\.");
        return this.outDir + items[0] + SCALE_SUFFIX + "." + items[1];
    }

    @Override
    public String toShCmd() {
        String sh = SHELL_VIDEO_SCALE_CMD.replace("INPUT_PATH", this.inputVideoPath);
        sh = sh.replace("VIDEO_FRAME", this.videoFrame.toString());
        sh = sh.replace("OUTPUT_PATH", this.getOutputPath());

        return sh;
    }


    public static void main(String[] args) {
        String inputVideoPath = "crop.png";
        VideoFrame videoFrame = new VideoFrame(1280, 720);
    }
}
