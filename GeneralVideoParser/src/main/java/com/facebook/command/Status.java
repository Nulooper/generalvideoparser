package com.facebook.command;

import java.io.Serializable;

/**
 * Created by zhiwei on 2018/1/10.
 */
public class Status implements Serializable {
    private int code = 0;
    private String message = "OK";

    public Status(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public Status() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Status{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}
