package com.facebook.command;

/**
 * 命令执行接口.
 * Created by zhiwei on 2018/1/10.
 */
public interface Executable {
    ExecuteResult execute();

    String toShCmd();
}
