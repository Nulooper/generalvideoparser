package com.facebook.command;

import com.facebook.shell.ShellHelper;
import com.facebook.utils.Constants;
import com.facebook.utils.Utils;

import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * 视频拼接命令.
 */
public class VideoConcatCommand implements Serializable, Executable {


    public static final String SHELL_VIDEO_CONCATE_CMD =
            Constants.SHELL_SCRIPTS_ROOT_DIR + "mmcat INPUT_PATH_LIST OUTPUT_PATH";

    private String outputConcatedPath;

    private List<String> inputVideoPathList;

    public VideoConcatCommand(List<String> inputVideoPathList, String outputConcatedPath) {
        this.outputConcatedPath = outputConcatedPath;
        this.inputVideoPathList = inputVideoPathList;
    }

    public VideoConcatCommand() {
    }

    public void addInputVideoPath(String inputVideoPath) {
        if (null != inputVideoPath) {
            if (null == this.inputVideoPathList) this.inputVideoPathList = new ArrayList<String>();
        }
        this.inputVideoPathList.add(inputVideoPath);
    }

    public String getOutputConcatedPath() {
        return outputConcatedPath;
    }

    public void setOutputConcatedPath(String outputConcatedPath) {
        this.outputConcatedPath = outputConcatedPath;
    }

    public List<String> getInputVideoPathList() {
        return inputVideoPathList;
    }

    public void setInputVideoPathList(List<String> inputVideoPathList) {
        this.inputVideoPathList = inputVideoPathList;
    }

    public String toShCmd() {

        String joinedInputVideoInfo = Utils.joinOn(this.inputVideoPathList, " ");
        String outputVideoPath = this.getOutputConcatedPath();
        String shCmd = SHELL_VIDEO_CONCATE_CMD.replace("INPUT_PATH_LIST", joinedInputVideoInfo);
        shCmd = shCmd.replace("OUTPUT_PATH", outputVideoPath);

        return shCmd;
    }

    @Override
    public ExecuteResult execute() {
        String shCmd = this.toShCmd();
        ExecuteResult executeResult = new ExecuteResult(this.getClass().getName(), shCmd);
        List<String> errorLineList = ShellHelper.exeShell(shCmd);
        executeResult.setErrorLineList(errorLineList);

        String outputConcatedPath = this.getOutputConcatedPath();
        boolean isVideoExists = Files.exists(Paths.get(this.getOutputConcatedPath()));
        if (!isVideoExists) {
            //目标文件不存在，则指令执行失败.
            Status status = new Status(-1, "合成视频[" + outputConcatedPath + "]失败!");
            executeResult.setStatus(status);
        }

        return executeResult;
    }

    @Override
    public String toString() {
        return "ConcatVideoCommand{" +
                "outputConcatedPath='" + outputConcatedPath + '\'' +
                ", inputVideoPathList=" + inputVideoPathList +
                '}';
    }

    public static void main(String[] args) {
    }
}
