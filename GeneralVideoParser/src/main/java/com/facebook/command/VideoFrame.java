package com.facebook.command;

import java.io.Serializable;

public class VideoFrame implements Serializable {
    private int width;
    private int height;

    public VideoFrame(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public VideoFrame() {
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public double getAspectRatio() {
        return this.width * 1.0 / this.height;
    }


    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        VideoFrame that = (VideoFrame) object;

        if (width != that.width) return false;
        return height == that.height;
    }

    @Override
    public int hashCode() {
        int result = width;
        result = 31 * result + height;
        return result;
    }

    @Override
    public String toString() {
        return this.width + ":" + this.height;
    }
}
